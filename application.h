/**
 * @file application.h
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 8 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license
 * This project is released under the GNU Public License (GPLv3).
 *
 * @brief Application header and configuration file.
 */

#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <stdint.h>

#include "solar-cartos/io/gpio/gpio.h"
#include "solar-cartos/io/spi/spi.h"

/*----------------------------
 * CONFIGURATION DEFINITIONS |
 *--------------------------*/
#define APP_SYSCLK_FREQ      48000000    ///< System clock frequency in Hz

/*-------------------------
 * SPI Bus Configurations |
 *-----------------------*/
static const SPI_config_t app_spis[] = {
 /* Bus,   Clock Source,  Clock Div, Clock Polarity,        Clock Phase            , Byte Order*/
 { SPI_B0, SPI_CLK_SMCLK, 1,         SPI_CPOL_0_ACTIVEHIGH, SPI_CPHA_0_CAPTUREFIRST, SPI_MSB_FIRST }
};

/*---------------------
 * PIN CONFIGURATIONS |
 *-------------------*/
enum {
    TEST_PIN,
    SPI_CS,
    SPI_SCLK,
    SPI_MOSI,
    SPI_MISO,
    GPIO_COUNT
};
static const GPIO_config_t app_pins[] = {
  /* Name,    Port,         Index,      Direction,   Function,     Pull Up/Down */
  { TEST_PIN, GPIO_PORT_3, GPIO_PIN_3, GPIO_OUTPUT, GPIO_FUNC_IO, GPIO_PULL_OFF },
  { SPI_CS,   GPIO_PORT_2, GPIO_PIN_4, GPIO_OUTPUT, GPIO_FUNC_IO, GPIO_PULL_UP },
  { SPI_SCLK, GPIO_PORT_1, GPIO_PIN_5, GPIO_OUTPUT, GPIO_FUNC_PRIMARY, GPIO_PULL_OFF },
  { SPI_MOSI, GPIO_PORT_1, GPIO_PIN_6, GPIO_OUTPUT, GPIO_FUNC_PRIMARY, GPIO_PULL_OFF },
  { SPI_MISO, GPIO_PORT_1, GPIO_PIN_7, GPIO_OUTPUT, GPIO_FUNC_PRIMARY, GPIO_PULL_OFF }
};

/*------------------------
 * FUNCTION DECLARATIONS |
 *----------------------*/
void appInit();
void appIdle(uint32_t args);

#endif /* APPLICATION_H_ */
