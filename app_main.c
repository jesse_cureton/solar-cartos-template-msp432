/**
 * @file app_main.c
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 8 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license
 * This project is released under the GNU Public License (GPLv3).
 *
 * @brief Application entry point & initialization
 */

#include "application.h"

#include "solar-cartos/include/error.h"
#include "solar-cartos/scheduler/scheduler.h"
#include "solar-cartos/io/gpio/gpio.h"
#include "solar-cartos/io/spi/spi.h"

void appInit()
{
    GPIO_init();
    SPI_init();

    // Ensure we can add a dummy application idle task that runs every second
    ASSERT(schAddTask(LOWEST, appIdle, NULL, 100, 1));
}

void appIdle(uint32_t args)
{
    static uint8_t val = 1;
    GPIO_toggle_pin(TEST_PIN);
    GPIO_write_pin(SPI_CS, false);
    SPI_transact(SPI_B0, val);
    GPIO_write_pin(SPI_CS, true);
    val++;
}
